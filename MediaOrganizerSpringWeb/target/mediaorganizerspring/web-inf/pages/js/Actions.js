var mediaObject;
var editMovieForm;
var languageData;
$("#search").typeahead({
	source : function(mediaName, process) {
		$.getJSON("MediaContents", {
			mediaType : "Movie",
			mediaTypeProperty : "Movie_Name",
			media : mediaName

		}, function(data) {
			mediaNames = [];

			$.each(data, function(mediaIndex, media, list) {
				console.log(media);
				mediaNames.push(media.name);
			});
			process(mediaNames);
		});
	},
	updater : function(mediaName) {
//		createMediaRows(mediaName);
	}
});

function createMediaRows(mediaName) {
	console.log("Running create media rows function");
	$.getJSON("MediaContents", {
		mediaType : "Movie",
		mediaTypeProperty : "Movie_Name",
		media : mediaName

	}, function(mediaJSON) {
//		mediaObject = mediaJSON;
//		console.log(mediaObject);
//		$(".media-row").remove();
//		$.each(mediaObject, function(mediaIndex,media) {
//
//			
//			var mediaName = media.name;
//			var mediaRating = media.rating;
//			var mediaGenre = media.genre;
//			var mediaDirector = media.director;
//
//			$("#search").val(mediaName);
//
//			var list_group_item = $('<a>', {
//				href : '#',
//				class : 'media-row list-group-item'
//			});
//			$('.list-group').append(list_group_item);
//
//			var media_element = $('<div>', {
//				class : 'media-element'
//			});
//			$(list_group_item).append(media_element);
//
//			var row = $('<div>', {
//				class : 'row'
//			});
//			$(media_element).append(row);
//
//			var edit_img = $('<img>', {
//				class : 'edit_image img-rounded img-responsive',
//				id : mediaName,
//				href : "#edit_modal",
//				src : "images/edit.png"
//			});
//			$(edit_img).attr("data-toggle", "modal");
//
//			$(row).append(edit_img);
//
//			var col_3 = $('<div>', {
//				class : 'col-3'
//			});
//			$(row).append(col_3);
//
//			var logo_img = $('<img>', {
//				class : 'media_image img-responsive',
//				src : "images/logo.png"
//			});
//			$(col_3).append(logo_img);
//
//			var col_9 = $('<div>', {
//				class : 'col-9'
//			});
//			$(row).append(col_9);
//
//			var col_5 = $('<div>', {
//				class : 'col-5',
//				text : languageData.name +" "+ mediaName
//			});
//			$(col_9).append(col_5);
//			var col_4 = $('<div>', {
//				class : 'col-4',
//				text : languageData.rating +" "+ mediaRating
//			});
//			$(col_9).append(col_4);
//			var div1 = $('<div>');
//			var genre_col = $('<div>', {
//				class : 'col-9',
//				text : languageData.genre +" "+ mediaGenre
//			});
//			$(div1).append(genre_col);
//
//			$(col_9).append(div1);
//			var div2 = $('<div>');
//			var director_col = $('<div>', {
//				class : 'col-9',
//				text : languageData.director +" "+ mediaDirector
//			});
//			$(div1).append(director_col);
//			$(col_9).append(div2);
//		});
//		bindEditImage();
	});
	return mediaName;
}

function changeLanguage(){
	if(languageData == null){
		var languageValue = $("#languageChange").val();
		$.get("ResourceBundle", {
			language : languageValue
		}, function(data) {
			languageData = data;
			reloadLabels();
		});	
	} else{
		reloadLabels();
	}
}

function reloadLabels(){
	$("#titleLabel").html(languageData.mediaOrganizer);
	$("#homeLabel").html(languageData.home);
	$("#importLabel").html(languageData.import);
	$("#logoutLabel").html(languageData.logout);
	$("#allMediaLabel").html(languageData.allMedia);
	$("#movieLabel").html(languageData.movie);
	$("#bookLabel").html(languageData.book);
	
	$("#movieNameLabel").html(languageData.movieName);
	$("#yearLabel").html(languageData.year);
	$("#ratingLabel").html(languageData.rating);
	$("#lengthLabel").html(languageData.length);
	$("#genreLabel").html(languageData.genre);
	$("#directorLabel").html(languageData.director);
	$("#actorsLabel").html(languageData.actors);
	$("#writersLabel").html(languageData.writers);
	$("#pathLabel").html(languageData.path);
	$("#plotLabel").html(languageData.plot);
	$("#getMovieData").html(languageData.getMediaData);
	$("#submitForm").html(languageData.save);
	$("#closeLabel").html(languageData.close);
	
	$("#changeLanguage").html(languageData.changeLanguage);
	$("#englishLabel").html(languageData.english);
	$("#frenchLabel").html(languageData.french);
}

$(window).load(function() {
	createMediaRows(null);
	console.log("Binding done!!");
	changeLanguage();
	
});

function bindEditImage() {
	$(".edit_image").on("click", function(event) {
		var movieName = event.target.id;
		console.log("Clicked")

		if(editMovieForm == null){
			$.get("EditMovie", function(data) {
				$("#edit_modal").html(data);
				editMovieForm = data;
			    loadEditMovieForm(editMovieForm, movieName);
			    bindEditMovieForm();
			    reloadLabels();
			});
		} else{
			loadEditMovieForm(editMovieForm, movieName);
		}
		
	});
}

function loadEditMovieForm(editMovieForm, movieName){
	$.each(mediaObject, function(i, media) {
		if (media.name == movieName) {
			$("#headerMovieName").html(movieName);

			$("#movieId").val(media.id);
			$("#movieName").val(movieName);
			$("#year").val(media.year);
			$("#rating").val(media.rating);
			$("#length").val(media.length);
			$("#genre").val(media.genre);
			$("#director").val(media.director);
			$("#writers").val(media.writers);
			$("#actors").val(media.actors);
			$("#path").val(media.path);
			$("#plot").val(media.plot);
		}
	});

	$("#getMovieData").on("click", function(event) {
		var movieName = $("#movieName").val();
		$.getJSON("http://www.imdbapi.com/", {
			t : movieName
		}, function(movie) {
			$("#movieName").val(movie.Title);
			$("#year").val(movie.Year);
			$("#rating").val(movie.imdbRating);
			$("#length").val(movie.Runtime);
			$("#genre").val(movie.Genre);
			$("#director").val(movie.Director);
			$("#writers").val(movie.Writer);
			$("#actors").val(movie.Actors);
			$("#plot").val(movie.Plot);
		});
	});
}


function bindEditMovieForm() {
	console.log("Binding Form");
	$("#submitForm").on("click", function() {

		var movieId = $("#movieId").val();
		var movieName = $("#movieName").val();
		var year = $("#year").val();
		var rating = $("#rating").val();
		var length = $("#length").val();
		var genre = $("#genre").val();
		var director = $("#director").val();
		var writers = $("#writers").val();
		var actors = $("#actors").val();
		var path = $("#path").val();
		var plot = $("#plot").val();

		$.get("EditMovieContents", {
			id : movieId,
			name : movieName,
			year : year,
			rating : rating,
			length : length,
			genre : genre,
			director : director,
			writers : writers,
			actors : actors,
			path : path,
			plot : plot
		}, function(data) {
			alert(data);
			createMediaRows(null);
			$('#edit_modal').modal('hide')
		});
	});
}

$(document).ready(function(){
	$("#languageChange").click(function(){
		languageData = null;
		changeLanguage();
	});
});