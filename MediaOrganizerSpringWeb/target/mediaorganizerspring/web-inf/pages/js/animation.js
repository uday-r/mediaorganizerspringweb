
function move(length){
    $(".content_wrapper").stop().animate({
        marginLeft: length
    },1600);
}

$(document).ready(function(){

    $("#pane1").click(function(){
        console.log("clicked1");
        move("0px");
    });

    /* Move about */
    $("#pane2").click(function(){
        console.log("clicked2");
        move("-"+screen.width+"px");
    });

    /* Move to Project */
    $("#pane3").click(function(){
        move("-"+screen.width*2+"px");
    });

});
