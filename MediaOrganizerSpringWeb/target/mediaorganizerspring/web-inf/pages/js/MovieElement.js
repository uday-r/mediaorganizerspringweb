angular.module('Media-Organizer', [ 'ui.bootstrap' ]);
function MovieDetailsElement($scope, $http, $compile) {

	$scope.moviesDetails = undefined;
	$scope.movieElement = undefined;
	$scope.init = function() {
		$http
				.get(
						"http://localhost:8080/MediaOrganizerSpringWeb/MediaContents?mediaType=Movie&mediaTypeProperty=Movie_Name&media=")
				.success(function(data) {

					$scope.moviesDetails = data;
					$scope.movieElement = $compile('<div>uday</div>')($scope);
					
				}).error(function(error) {
					console.log(error);
				})
	}
}