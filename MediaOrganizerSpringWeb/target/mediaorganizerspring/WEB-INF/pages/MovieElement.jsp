<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<a href="#" class="media-row list-group-item"><div
			class="media-element">
			<div class="row">
				<img class="edit_image img-rounded img-responsive" id="Thor"
					href="#edit_modal" src="images/edit.png" data-toggle="modal">
				<div class="col-3">
					<img class="media_image img-responsive" src="images/logo.png">
				</div>
				<div class="col-9">
					<div class="col-5">Name: Thor</div>
					<div class="col-4">Rating: 7</div>
					<div>
						<div class="col-9">Genre: Action, Adventure, Fantasy</div>
						<div class="col-9">Director: Kenneth Branagh</div>
					</div>
					<div></div>
				</div>
			</div>
		</div></a>
</body>
</html>