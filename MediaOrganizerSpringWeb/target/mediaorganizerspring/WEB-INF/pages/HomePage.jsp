<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html ng-app="Media-Organizer">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/bootstrap1.css" rel="stylesheet">
<link href="css/sticky-footer-navbar.css" rel="stylesheet"
	media="screen">
<link href="css/MainStyle.css" rel="stylesheet" media="screen">
</head>
<body>
	<div id="wrap">
		<!-- Fixed navbar -->
		<div class="navbar">
			<div class="container">
				<a class="navbar-brand" id="titleLabel" href="#"></a>
				<div class="nav-collapse collapse navbar-responsive-collapse">
					<div class="nav-collapse collapse">
						<ul class="nav navbar-nav">
							<li id="pane1"><a href="#pane1" id="homeLabel"
								data-toggle="tab"></a></li>
							<li id="pane2"><a href="#pane2" id="importLabel"
								data-toggle="tab"></a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
					<ul class="nav navbar-nav pull-right">
						<li><a href="#">${username}</a></li>
						<li><a href="logout" id="logoutLabel"></a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Begin page content -->
		<div class="container">
			<div class="content_wrapper">
				<div class="pane">
					<div class="media_list list-group">
						<div href="#" class="list-group-item">
							<div class="row">
								<div ng-controller="MovieFilter">
									<div class="pull-left col-7">
										<input type="text" ng-model="movieNameSearch"
											typeahead="movie for movie in movies| filter:$viewValue | limitTo:8"
											class="ng-valid ng-dirty ng-valid-editable">
									</div>
									<div class="pull-right col-4">
										<select id="media-type" class="form-control">
											<option value="allMedia" id="allMediaLabel">All
												Media</option>
											<option value="movie" id="movieLabel">Movie</option>
											<option value="book" id="bookLabel">Book</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div ng-controller="MovieDetailsElement" ng-init="init()">
							<div ng-repeat="movieDetails in moviesDetails">
								<a href="#">{{movieElement}}</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>




	<div id="footer">
		<div id="wrapper" class="container-fluid">
			<div class="row-fluid">
				<form id="frmOptions" method="post" class="form-inline">
					<div class="row-fluid">
						<div id="formLeft" class=" span4">
							<div class="pull-right">
								<div class="control-group">
									<div class="controls">
										<label for="select1" id="changeLanguage" class="control-label"></label>
										<select id="languageChange">
											<option id="englishLabel" value="en"></option>
											<option id="frenchLabel" value="fr"></option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal" id="edit_modal"></div>


	<script src="js/jquery.js"></script>
	<script src="js/angular.js"></script>
	<script src="js/ui-bootstrap-tpls-0.6.0.js"></script>
	<script
		src="http://static.scripting.com/github/bootstrap2/js/bootstrap-modal.js"></script>
	<script src="js/bootstrap-typeahead.js"></script>

	<script src="js/Actions.js"></script>
	<script type="text/javascript" src="js/MainController.js"></script>
	<script type="text/javascript" src="js/MovieNameFilter.js"></script>
	<script type="text/javascript" src="js/MovieElement.js"></script>
	<script type="text/javascript">
		var userName = "${username}";
		console.log(userName);
	</script>
</body>
</html>