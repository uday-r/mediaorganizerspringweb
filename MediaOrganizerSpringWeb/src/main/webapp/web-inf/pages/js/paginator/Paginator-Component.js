function getMediaPropertyValueIfNull(mediaPropertyValue) {
	if (mediaPropertyValue == null) {
		mediaPropertyValue = $("#search").val();
	}
	return mediaPropertyValue;
}

function pagination(mediaPropertyValue) {

	mediaPropertyValue = getMediaPropertyValueIfNull(mediaPropertyValue);

	var mediaType = $("#media-type").val();
	var mediaTypeProperty = $("#search-by").val();
	var media = mediaPropertyValue;
	$.get("MediaContents", {
		mediaType : mediaType,
		mediaTypeProperty : mediaTypeProperty,
		media : media,
		interval : $("#resultsPerPage").val(),
		pageNo : 1
	}, function(data) {
		var mediaCount = data.Data.mediaCount;
		mediaObject.mediaCount = mediaCount;
		var resultsPerPage = $("#resultsPerPage").val();
		var pagesRequired = Math.ceil(mediaCount / resultsPerPage);
		currentPage = 1;
		paginatorSetup(currentPage, pagesRequired, data);
	});
}

function paginatorSetup(currentPage, pagesRequired, data) {
	var mediaType = $("#media-type").val();
	var mediaTypeProperty = $("#search-by").val();
	var media = $("#search").val();

	$('#pagination').bootstrapPaginator({
		currentPage : currentPage,
		totalPages : pagesRequired,
		itemContainerClass : function(type, page, current) {
			return (page === current) ? "active" : "pointer-cursor";
		},
		onPageClicked : function(e, originalEvent, type, pageNo) {

			var resultsPerPage = $("#resultsPerPage").val();
			var currentResults = resultsPerPage * pageNo;
			var currentPageFirstIndex = resultsPerPage * (pageNo - 1);
			if (mediaObject.mediaList[currentPageFirstIndex] == undefined) {
				$.get("MediaContents", {
					mediaType : mediaType,
					mediaTypeProperty : mediaTypeProperty,
					media : media,
					interval : $("#resultsPerPage").val(),
					pageNo : pageNo
				}).done(function(returnData) {
					searchController.paginationIsDone(returnData.Data.mediaList, pageNo);
				});
			} else {
				searchController.paginationIsDone(null, pageNo);
			}
		}
	});
	searchController.paginationIsDone(data.Data.mediaList, currentPage);
}