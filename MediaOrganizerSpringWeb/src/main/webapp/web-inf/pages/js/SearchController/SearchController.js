function SearchController(){
	
	this.typeaheadIsDone = function(mediaValue){
		pagination(mediaValue);
	}
	
	this.paginationIsDone = function(mediaList, pageNo){
		createMediaRows(mediaList,pageNo);
	}
}