$.fn.typeahead.Constructor.prototype.process = function(items) {
	var that = this

	items = $.grep(items, function(item) {
		return that.matcher(item)
	})

	items = this.sorter(items)
	if (!items.length) {
		return this.rendernonmatch().show()
	}

	return this.render(items.slice(0, this.options.items)).show()
}

$.fn.typeahead.Constructor.prototype.rendernonmatch = function() {
	var that = this
	var items = new Array();
	if ($("#search-by").val().length == 0) {
		items.push(matchCriteriaError);
	} else {
		items.push(matchError)
	}

	items = $(items).map(function(i, item) {
		i = $(that.options.item).attr('data-value', item)
		i.find('a').html(that.highlighter(item))
		return i[0]
	})
	items.first().addClass('active')
	this.$menu.html(items)
	return this
}

$("#search").typeahead({

	
	source : function(mediaName, process) {

		$.getJSON("SearchMedia", {
			mediaType : $("#media-type").val(),
			mediaTypeProperty : $("#search-by").val(),
			media : mediaName
		}, function(data) {
			var mediaPropertyValues = new Array();
			if (data.Data != undefined) {
				mediaList = data.Data.mediaList;
				$.each(mediaList, function(mediaIndex, media, list) {

					var mediaProperty = $('#search-by').val().toLowerCase();
					mediaPropertyValues.push(media);

				});
				mediaPropertyValues = $.unique(mediaPropertyValues);
			}
			process(mediaPropertyValues);
		});
	},

	updater : function(mediaValue) {
		mediaObject.mediaCount=0;
		mediaObject.mediaList=new Array();
		mediaValue = checkIfMessageInData(mediaValue);
		if(mediaValue !=null){
			searchController.typeaheadIsDone(mediaValue);
		}
		return mediaValue;
	}
});

function checkIfMessageInData(mediaPropertyValue){
	if(mediaPropertyValue=="No Match Found" || mediaPropertyValue=="Please Select search criteria"){
		if(mediaPropertyValue=="Please Select search criteria"){
			$("#search-by").focus();
		}
		return null;
	} else {
		return mediaPropertyValue;
	}
}

