function popupStatusMessage(data) {
	var updateStatus = data.Result;
	var selectStatus = data.Data.Result;

	if (selectStatus == "Success") {
		alert(updateStatus);
	} else {
		alert(updateStatus + ".\n" + data.Data.Message);
	}
}

function parseReloadHref(movieObject) {
	$.each($("a.media-row.list-group-item"), function(aIndex, element) {
		if (element.getAttribute("movie-id") == movieObject[0].id) {
			$(element).find(".rowMovieName .value").text(movieObject[0].name);
			$(element).find(".rowMovieRating .value").text(
					movieObject[0].rating);
			$(element).find(".rowMovieGenres .value").text(
					movieObject[0].genres);
			$(element).find(".rowMovieDirector .value").text(
					movieObject[0].director);
		}
	});

}

function getMediaListIfNull(mediaList ,currentPageFirstIndex ,currentResults){
	if(mediaList == null){
		mediaList = mediaObject.mediaList.slice(currentPageFirstIndex,currentResults);
	}
	return mediaList;
}

function createMediaRows(mediaList, pageNo) {

	$(".media-row").remove();

	var resultsPerPage = $("#resultsPerPage").val();
	var currentResults = resultsPerPage*pageNo;
	var currentPageFirstIndex=resultsPerPage*(pageNo-1);

	mediaList=getMediaListIfNull(mediaList ,currentPageFirstIndex ,currentResults);
	
	$.each(mediaList, function(mediaIndex, media) {
		if(mediaObject.mediaList[currentPageFirstIndex] == undefined){
			mediaObject.mediaList[currentPageFirstIndex] = media;
			currentPageFirstIndex++;
		}

		if(movieRowHtml==null){
			$.get("MovieRow.html", function(html) {
				movieRowHtml = html;
				populateRowData(media, bindEditImage);
			})
		} else {
			populateRowData(media, bindEditImage);
		}
	});
}

function populateRowData(media, bindEditImage){
	html = $.parseHTML(movieRowHtml);
	$(html).attr("movie-id", media.id);
	$(html).find(".edit_image").attr("movie-id", media.id);
	$(html).find(".rowMovieName .value").text(media.name);
	$(html).find(".rowMovieRating .value").text(media.rating);
	$(html).find(".rowMovieGenres .value").text(media.genres);
	$(html).find(".rowMovieDirector .value").text(media.director);
	$(".media_list").append(html);
	bindEditImage();
}

function bindEditImage() {
	$(".edit_image").on("click", function(event) {
		var movieId = event.target.getAttribute("movie-id");
		var mediaDetails;

		$.each(mediaObject.mediaList, function(mediaIndex, media) {
			if (media["id"] == movieId) {
				mediaDetails = media;
			}
		});
		if (editMovieForm == null) {
			$.get("EditMovie.html", function(data) {
				$("#edit_modal").html(data);
				editMovieForm = data;
				loadEditMovieForm(editMovieForm, mediaDetails);
				bindEditMovieForm(mediaDetails);
				reloadLabels();
			});
		} else {
			loadEditMovieForm(editMovieForm, mediaDetails);
		}

	});
}

function loadEditMovieForm(editMovieForm, movieDetails) {
	$("#headerMovieName").html(movieDetails.name);
	$("#editFormMovieId").val(movieDetails.id);
	$("#editFormMovieName").val(movieDetails.name);
	$("#editFormMovieYear").val(movieDetails.year);
	$("#editFormMovieRating").val(movieDetails.rating);
	$("#editFormMovieLength").val(movieDetails.length);
	$("#editFormMovieGenres").val(movieDetails.genres);
	$("#editFormMovieDirector").val(movieDetails.director);
	$("#editFormMovieWriters").val(movieDetails.writers);
	$("#editFormMovieActors").val(movieDetails.actors);
	$("#editFormMoviePath").val(movieDetails.path);
	$("#editFormMoviePlot").val(movieDetails.plot);
}

function bindEditMovieForm(movieDetails) {
	console.log("Binding Form");
	
	$("#getMovieData").on("click", function(event) {
		event.preventDefault();
		if(navigator.online){
			var ladda = Ladda.create(this);
			ladda.start();
			var movieName = movieDetails.name;
			$.getJSON("http://www.imdbapi.com/", {
				t : movieName
			}, function(movie) {
				$("#editFormMovieName").val(movie.Title);
				$("#editFormMovieYear").val(movie.Year);
				$("#editFormMovieRating").val(movie.imdbRating);
				$("#editFormMovieLength").val(movie.Runtime);
				$("#editFormMovieGenres").val(movie.Genre);
				$("#editFormMovieDirector").val(movie.Director);
				$("#editFormMovieWriters").val(movie.Writer);
				$("#editFormMovieActors").val(movie.Actors);
				$("#editFormMoviePlot").val(movie.Plot);
				ladda.stop();
			});	
		} else{
			alert("Please check the internet connection");
		}
		
	});
	
	
	$("#submitForm").on("click", function(event) {
		event.preventDefault();
		var ladda = Ladda.create(this);
		ladda.start();
		var movieId = $("#editFormMovieId").val();
		var movieName = $("#editFormMovieName").val();
		var year = $("#editFormMovieYear").val();
		var rating = $("#editFormMovieRating").val();
		var length = $("#editFormMovieLength").val();
		var genres = $("#editFormMovieGenres").val();
		var director = $("#editFormMovieDirector").val();
		var writers = $("#editFormMovieWriters").val();
		var actors = $("#editFormMovieActors").val();
		var path = $("#editFormMoviePath").val();
		var plot = $("#editFormMoviePlot").val();

		$.get("EditMovieContents", {
			id : movieId,
			name : movieName,
			year : year,
			rating : rating,
			length : length,
			genres : genres,
			director : director,
			writers : writers,
			actors : actors,
			path : path,
			plot : plot
		}, function(data) {
			ladda.stop();
			popupStatusMessage(data);
			parseReloadHref(data.Data.Data.mediaList);
			var movies = new Array(data.Data);
			$('#edit_modal').modal('hide')
		});
	});
}
