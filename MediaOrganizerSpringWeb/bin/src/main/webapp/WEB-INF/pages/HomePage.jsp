<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link
	href="css/bootstrap1.css"
	rel="stylesheet">
<link href="css/sticky-footer-navbar.css" rel="stylesheet"
	media="screen">
<link href="css/MainStyle.css" rel="stylesheet" media="screen">
</head>
<body>
	<div id="wrap">
		<!-- Fixed navbar -->
		<div class="navbar">
			<div class="container">
				<a class="navbar-brand" id="titleLabel" href="#"></a>
				<div class="nav-collapse collapse navbar-responsive-collapse">
					<div class="nav-collapse collapse">
						<ul class="nav navbar-nav">
							<li id="pane1"><a href="#pane1" id="homeLabel"
								data-toggle="tab"></a></li>
							<li id="pane2"><a href="#pane2" id="importLabel"
								data-toggle="tab"></a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
					<ul class="nav navbar-nav pull-right">
						<li><a href="#">${username}</a></li>
						<li><a href="logout" id="logoutLabel"></a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Begin page content -->
		<div class="container">
			<div class="content_wrapper">
				<div class="pane">
					<div class="media_list list-group">
						<div href="#" class="list-group-item">
							<div>
								<div class="row">
									<div class="col-6">
										<input type="text" autocomplete="off" class="form-control"
											id="search" data-provide="typeahead">
									</div>
									<div class="col-3 col-3-offset"></div>
									<div class="col-3">
										<select id="media-type" class="form-control">
											<option value="allMedia" id="allMediaLabel"></option>
											<option value="movie" id="movieLabel"></option>
											<option value="book" id="bookLabel"></option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<a href="#" class="media-row list-group-item"> </a> <a href="#"
							class="media-row list-group-item"> </a>
					</div>
				</div>
			</div>
		</div>
	</div>




	<div id="footer">
		<!-- <div class="container">
			<div class="pull-right">
				<div >
					<label for="change-language" class="control-label">Language
						Change:</label>
				</div>
				<div class="span3">
					<select id="change-language">
						<option value>English</option>
						<option value>French</option>
					</select>
				</div>
			</div>
		</div>-->
		<div id="wrapper" class="container-fluid">
			<div class="row-fluid">
				<form id="frmOptions" method="post" class="form-inline">
					<div class="row-fluid">
						<div id="formLeft" class=" span4">
							<div class="pull-right">
								<div class="control-group">
									<div class="controls">
										<label for="select1" id="changeLanguage" class="control-label"></label> <select id="languageChange">
											<option id="englishLabel" value="en"></option>
											<option id="frenchLabel" value="fr"></option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal" id="edit_modal"></div>


	<!-- JavaScript plugins (requires jQuery) -->
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/jquery.js"></script>
	<!-- <script src="js/jquery-1.9.1.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap.min.js"></script> -->
	<script
		src="http://static.scripting.com/github/bootstrap2/js/bootstrap-modal.js"></script>
	<script src="js/animation.js"></script>
	<script src="js/bootstrap-typeahead.js"></script>
	<script src="js/Actions.js"></script>
	<script type="text/javascript">
	var userName = "${username}";
	console.log(userName);
	</script>
</body>
</html>