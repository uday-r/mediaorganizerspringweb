
<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<center>
		<h2 id="headerMovieName"></h2>
	</center>
</div>
<div class="modal-body">
	<form class="form-horizontal">
		<fieldset>
			<div class="control-group">
				<label class="control-label" id="movieNameLabel" for="movieName"></label>
				<div class="controls">
					<input style="display: none" type="text" id="movieId"> <input
						type="text" id="movieName" placeholder="Movie Name">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" id="yearLabel" for="year"></label>
				<div class="controls">
					<input type="text" id="year" placeholder="Year">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" id="ratingLabel" for="rating"></label>
				<div class="controls">
					<input type="text" id="rating" placeholder="Rating">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" id="lengthLabel" for="length"></label>
				<div class="controls">
					<input type="text" id="length" placeholder="Length">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" id="genreLabel" for="genre"></label>
				<div class="controls">
					<input type="text" id="genre" placeholder="Genre">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" id="directorLabel" for="director"></label>
				<div class="controls">
					<input type="text" id="director" placeholder="Director">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" id="actorsLabel" for="actors"></label>
				<div class="controls">
					<input type="text" id="actors" placeholder="Actors">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" id="writersLabel" for="writers"></label>
				<div class="controls">
					<input type="text" id="writers" placeholder="Writers">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" id="pathLabel" for="path"></label>
				<div class="controls">
					<input type="text" id="path" class="uneditable-input"
						placeholder="Path">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" id="plotLabel" for="plot"></label>
				<div class="controls">
					<input type="text" id="plot" placeholder="Plot">
				</div>
			</div>
		</fieldset>
	</form>

</div>
<div class="modal-footer">
	<a href="#" id="getMovieData" class="btn btn-primary">Get Media
		Data</a> <a href="#" id="submitForm" type="submit" class="btn btn-primary">Save
		Changes</a> <a href="#" class="btn" id="closeLabel" data-dismiss="modal">Close</a>
</div>